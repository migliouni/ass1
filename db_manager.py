import mysql.connector
import time
# prima connessione al db, se non esiste si attende 20 secondi, per dare tempo al container con 
# mysql di essere attivo
def first_conn():
    try:
        conn_db()
    except:
        time.sleep(50)

# creazione del database se non presente
def create_db():
    mydb = mysql.connector.connect(
    host="mysqldb",
    user="root",
    password="root")
    mycursor = mydb.cursor()
    mycursor.execute("CREATE DATABASE IF NOT EXISTS mydatabase;")
    mydb= conn_db()
    mycursor = mydb.cursor()
    mycursor.execute("CREATE TABLE IF NOT EXISTS studenti (matricola int(10) NOT NULL, nome varchar(50) NOT NULL, cognome varchar(50) NOT NULL, PRIMARY KEY (matricola))")
    mycursor.execute("CREATE TABLE IF NOT EXISTS voti (id int(10) NOT NULL AUTO_INCREMENT,materia varchar(50) NOT NULL,voto int(2) NOT NULL,studente int(10) NOT NULL,PRIMARY KEY (id))") 


# connessione vera e proprio al database esistente per poter eseguire le query
def conn_db():
    mydb = mysql.connector.connect(
    host="mysqldb",
    user="root",
    password="root",
    database="mydatabase")
    return mydb

# inserimento nelle due tabelle delle tuple passate      
def insert(tupla_st, tupla_voti):
    mydb = conn_db()
    mycursor = mydb.cursor()
    if checkNewMat(tupla_st[0][0]):
        sql = "INSERT INTO studenti (matricola, nome, cognome) VALUES (%s, %s, %s)"
        mycursor.executemany(sql, tupla_st)
        mydb.commit()
    sql = "INSERT INTO voti (materia, voto, studente) VALUES (%s, %s, %s)"
    mycursor.executemany(sql, tupla_voti)
    mydb.commit()
    
# selezione dei voti presenti nel db della matricola passata in input
def select_voti(matricolaSt):
    mydb = conn_db()
    mycursor = mydb.cursor()
    sql = "SELECT materia, voto FROM voti WHERE studente = %s"
    mat = (matricolaSt, )
    mycursor.execute(sql, mat)
    myresult = mycursor.fetchall()
    i=1
    for x in myresult:
        print(str(i) + ") " + str(x[0]) + " " + str(x[1]))
        i=i+1
    return myresult

# Restituisce nome e cognome dello studente passando la matricola
def select_nome_cognome(matricolaSt):
    mydb = conn_db()
    mycursor = mydb.cursor()
    sql = "SELECT nome, cognome FROM studenti WHERE matricola = %s"
    mat = (matricolaSt, )
    mycursor.execute(sql, mat)
    myresult = mycursor.fetchall()
    return myresult
    

# controllo della matricola, sè già presente nella tabella studenti non 
# viene reinserita in quella tabella, ma solo nella tabella voti
def checkNewMat(matricola):
    mydb = conn_db()
    mycursor = mydb.cursor()
    sql = "SELECT matricola FROM studenti WHERE matricola = %s"
    mat = (matricola, )
    mycursor.execute(sql, mat)
    myresult = mycursor.fetchone()
    if myresult != None:
        return False
    else:
        return True

# elimina il contenuto delle due tabelle
def svuota_db():
    mydb = conn_db()
    mycursor = mydb.cursor()
    mycursor.execute("DELETE FROM studenti")
    mycursor.execute("DELETE FROM voti")
    mydb.commit()

# verifica che il corso non sia già stato verbalizzato
def checkCorso(matricola, corso):
    mydb = conn_db()
    mycursor = mydb.cursor()
    sql = "SELECT id FROM voti WHERE studente = %s AND materia = %s "
    par = (matricola, corso )
    mycursor.execute(sql, par)
    myresult = mycursor.fetchone()
    if myresult == None:
        return True
    else:
        print("Esame già verbalizzato!")
        return False
    
    


