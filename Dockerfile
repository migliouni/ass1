FROM python:3.6

WORKDIR /app

COPY requirements.txt /app
RUN apt-get update
RUN pip install -r requirements.txt

EXPOSE 80

COPY main.py /app
COPY db_manager.py /app

ENTRYPOINT ["python", "main.py"]
