# ass1
Assignment 1: DevOps
Gruppo composto da:
Andrico Michele
Miglio Diego
Mat. 810946
Mat. 807408
Repository GitLab: https://gitlab.com/migliouni/ass1/

Applicazione
L’applicazione sviluppata permette l’inserimento di voti di esami universitari in un database MySQL tramite riga
di comando. All’utente viene richiesto di inserire:
• Matricola;
• Nome e cognome;
• Corso;
• Voto;

Successivamente viene mostrata a schermo la tabella contenente tutti gli esami registrati dello studente, con i
relativi voti. L’applicazione è implementata in Python 3.6 ed è composta da due file principali: “main.py”, che
contiene la logica per l’inserimento e il controllo dell’input, e dal file “db_manager.py”, che contiene il codice
necessario ad effettuare le operazioni sul database.
Dopo uno studio delle principali caratteristiche di un sistema DevOps, si è deciso di implementare gli aspetti di
containerization, e continuous integration/continuous development.

Aspetto 1: Containerization
Utilizzando Docker si è inizialmente creato in locale un container Python, chiamato “myappt”. Si è andato, poi, a
creare un secondo container (“mysqldb”) partendo dall’immagine ufficiale di MySQL 5.7 disponibile su
DockerHub. Questo container viene eseguito in background attendendo che l’applicazione Python vi interagisca.
A questo punto sono stati collegati i due container utilizzando l’opzione “--link” del comando “docker run”. Il
sistema risulta in questo modo distribuito, in quanto permette l’interazione tra due container differenti.

Aspetto 2: Continuous integration/continuous development
Dopo aver testato l’effettivo funzionamento dell’applicazione containerizzata in locale, si è deciso di sfruttare la
funzione di CI/CD integrata nativamente in GitLab. Ѐ stato necessario, quindi, installare un runner GitLab su una
macchina Ubuntu in locale. In questo modo, ogni volta che uno sviluppatore esegue un push di una nuova
versione sulla repository GitLab i file vengono aggiornati e viene eseguito il file “.gitlab-ci.yml”, che descrive la
serie di comandi che il runner deve eseguire, ottenendo così l’implementazione di continuous integration e
continuous development. Direttamente dalla pagina GitLab del progetto, si può, inoltre, vedere lo stato in
diretta della pipeline, In particolare sono presenti due stage: build, dove viene creata l’immagine della nostra
applicazione, e test, dove vengono eseguiti i test cases automatici presenti nel main.

Aspetto 3: Migrazione del runner su cloud
Si è deciso infine di installare il runner GitLab, necessario per il continuous integration e il continuous testing, in
una macchina virtuale in remoto su cloud. Ѐ stata utilizzata una virtual machine Ubuntu 16.04.5 con 8 GB di
RAM e processore Intel Xeon E3-12xx v2, concessa in prova per 6 mesi dalla piattaforma cloud Garr. In questo
modo si è potuto evitare di mantenere costantemente accesa e collegata alla rete la macchina fisica su cui
risiedeva precedentemente il runner, costatando inoltre un aumento considerevole delle prestazioni.

Aspetto 4: Deployment
Si è deciso infine di integrare il deployment automatico dell’immagine della nostra applicazione su Docker Hub, in modo da ottenere una pipeline completa ad ogni commit dell’applicazione sulla repository GitLab. In primo luogo ci si è registrati su “hub.docker.com” e successivamente si è creata una repository su cui eseguire il deploy (“807810/repoprocesso”). Allo scopo di integrare il deploy è stato aggiunto uno stage chiamato “deploy” alla pipeline descritta nel file “.gitlab-ci.yml” che viene eseguita dal runner. In questo stage viene prima creata l’immagine dell’applicazione, quindi eseguito il login alla repository Docker Hub e, successivamente,eseguito un push di questa immagine su di essa. A questo punto l’utente interessato all’utilizzo dell’applicazione deve scaricare l’immagine dell’applicazione dalla repository pubblica “807810/repoprocesso”, lanciare un’immagine MySQL ed infine eseguire le due immagini collegate. Allo scopo di semplificare questo procedimento per l’utente, si è creato un file BASH “execApp.sh” che è stato anch’esso pubblicato sulla repository GitLab. Questo file, se lanciato da terminale, esegue automaticamente questa procedura rendendo l’applicazione usabile anche da utenti non esperti.

Aspetto 5: Sicurezza e prestazioni
Fino a questo momento le credenziali necessarie a creare l’immagine di MySQL e collegarsi a Docker Hub risultavano in chiaro, compromettendo la sicurezza del sistema. Per evitare questa falla si è deciso quindi di sfruttare le variabili protette fornite da GitLab per l’ambiente CI/CD. Nel file “.gitlab-ci.yml”, in questo modo, sono visibili solamente i riferimente a queste variabili, nascondendo il loro contenuto. Per migliorare le prestazioni del processo, oltre ad utilizzare una macchina virtuale dedicata per il runner, si è creata e pubblicata sulla repository “807810/repoprocesso” una seconda immagine con tag “build” contenente solamente l’applicazione, in modo da poterla riutilizzare nello stage di test senza doverla necessariamente ricreare da zero.


