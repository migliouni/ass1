import db_manager
import sys 
import os
def main():
    #prima connessione al db
    db_manager.first_conn()
    
    #creazione di db se non esiste ed inserimento valori
    db_manager.create_db()
    
    #richiesta in input valori e verificata validità
    try:
        print(os.linesep + "Benvenuto nell'applicazione per gestione voti degli studenti")
        matricola = input("Inserire numero matricola: ")
        aMatricola(matricola)
        if db_manager.checkNewMat(matricola) == True:
            nome = input("Inserire nome studente: ")
            aNomeCognome(nome)
            cognome = input("Inserire cognome studente: ")
            aNomeCognome(cognome)
        else:
            res = db_manager.select_nome_cognome(matricola)
            nome = res[0][0]
            cognome = res[0][1]
            print("Nome: " + nome)
            print("Cognome: " + cognome) 
        
        corso = input("Inserire nome corso: ")
        aCorso(matricola,corso)
        voto =  input("Inserire voto: ")
        aVoto(voto)
        #print("Esame già verballizato")
    except AssertionError:
        print("Dati richiesti inseriti in modo scorretto!")
        quit()
    
    #creazione tuple da inserire nel db
    tuplaStudente = [(matricola, nome, cognome)]
    tuplaVoto = [(corso, voto, matricola)]
    
    print("Elenco dei voti della matricola " + str(matricola) + ":")
    
    db_manager.insert(tuplaStudente, tuplaVoto)
   
    #query di selezione di tutti i voti della matricola 
    db_manager.select_voti(matricola) 

#casi di test automatici
def test_cases(): 
    db_manager.first_conn()
    db_manager.create_db()
    #db_manager.svuota_db()
	
    #inserimento valido nel db
    test1 = ["450908", "Daniele", "Rossi", "Basi di Dati", "18"]
    
    #check validità degli attributi
    aMatricola(test1[0])
    aNomeCognome(test1[1])
    aNomeCognome(test1[2])
    aVoto(test1[4])
    
    db_manager.create_db()
    tuplaStudente = [(test1[0],test1[1],test1[2])]
    tuplaVoto = [(test1[3],test1[4],test1[0])]
    db_manager.insert(tuplaStudente, tuplaVoto)
    voti = db_manager.select_voti(test1[0])
    
    #check tupla inserita nel db correttamente
    try:
        assert(voti[0][0] == 'Basi di Dati')
        assert(voti[0][1] == 18)
        print("Test inserimento valido PASSATO")
    except AssertionError:
        print("Test inserimento valido NON PASSATO")
    
    try:
        test2 = ["450", "Filippo", "verdi", "Basi di Dati 2", "23"]
        aMatricola(test2[0])
        aNomeCognome(test2[1])	    
        aNomeCognome(test2[2])
        aVoto(test2[4])
    except AssertionError:
        print("Test inserimento non valido, PASSATO matricola lunga solo 3 caratteri")
    
    #inserimento non valido matricola con caratteri non numerici
    try:
        test3 = ["45s999", "Loris", "neri", "Algoritmi", "26"]
        aMatricola(test3[0])
        aNomeCognome(test3[1])
        aNomeCognome(test3[2])
        aVoto(test3[4])
    except AssertionError:
        print("Test inserimento non valido, PASSATO matricola con caratteri non numerici")
    
    #inserimento non valido nome con caratteri numerici	
    try:
        test4 = ["112999", "22Loris", "neri", "Algoritmi", "26"]
        aMatricola(test4[0])
        aNomeCognome(test4[1])
        aNomeCognome(test4[2])
        aVoto(test4[4])
    except AssertionError:
        print("Test inserimento non valido, PASSATO nome con caratteri numerici")
    
    #inserimento non valido voto > 30
    try:
        test5 = ["999666", "Loris", "neri", "Algoritmi", "35"]
        aMatricola(test5[0])
        aNomeCognome(test5[1])
        aNomeCognome(test5[2])
        aVoto(test5[4])
    except AssertionError:
        print("Test inserimento non valido, PASSATO voto > 30")
    
    #inserimento valido nel db con studente già presente, stamperà i due suoi voti
    test6 = ["450908", "Daniele", "Rossi", "Analisi", "19"]
    
    #check validità degli attributi
    aMatricola(test6[0])
    aNomeCognome(test6[1])
    aNomeCognome(test6[2])
    aVoto(test6[4])
    
    tuplaStudente = [(test6[0],test6[1],test6[2])]
    tuplaVoto = [(test6[3],test6[4],test6[0])]
    db_manager.insert(tuplaStudente, tuplaVoto)
    voti = db_manager.select_voti(test6[0])

#check matricola
def aMatricola(matricola):
    assert(matricola.isdigit() == True)
    assert(len(matricola) == 6)

#check nome/cognome
def aNomeCognome(valore):
    assert(len(valore) == len(valore.strip("0,1,2,3,4,5,6,7,8,9")))

#check corso già verbalizzato
def aCorso(matricola, corso):
    assert(db_manager.checkCorso(matricola, corso) == True)

#check voto
def aVoto(voto):
    assert(voto.isdigit() == True)
    assert(len(voto) == 2)
    assert((int(voto) >= 18) and (int(voto) <= 31))

if __name__ == "__main__":
    if (len(sys.argv) > 1) and (sys.argv[1] == "test"):
        test_cases()
    else:
        main()    


	
	
	
